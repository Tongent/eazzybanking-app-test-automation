package com.application.libraries;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

public class Screenshot {

		public static void getscreenshot(WebDriver driver,String screenshotName)
		{
			
			TakesScreenshot ts=(TakesScreenshot)driver;
		    File source= ts.getScreenshotAs(OutputType.FILE);
		    try {
				FileHandler.copy(source, new File("./ScreenshotOfAndroid/"+screenshotName+".png"));
			} catch (Exception e) {
				System.out.println("Exception while taking screenshot"+e.getMessage());
			}
		}
		

}
