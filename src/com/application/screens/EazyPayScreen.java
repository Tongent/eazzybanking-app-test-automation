package com.application.screens;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.android.AndroidKeyCode;

public class EazyPayScreen {

AndroidDriver<?> driver;
	
	public EazyPayScreen(AndroidDriver<?> driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'login')]")
	private WebElement navigatetoLoginbutton;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'1')]")
	private WebElement navigatetoPIN1;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'2')]")
	private WebElement navigatetoPIN2;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'3')]")
	private WebElement navigatetoPIN3;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'4')]")
	private WebElement navigatetoPIN4;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btnSkip')]")
	private WebElement navigatetoSkip;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'close_button')]")
	private WebElement navigatetoClose;
	
	@FindBy(xpath = "//android.widget.ImageButton[contains(@resource-id,'fab_expand_menu_button')]")
	private WebElement navigatetoAddButton;

	@FindBy(xpath = "//android.widget.ImageButton[contains(@resource-id,'button_eazzypay')]")
	private WebElement navigatetoEazyPay;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Pay Bill')]")
	private WebElement navigatetoPayBill;
	
	//Search field
//	@FindBy(xpath = "//android.widget.EditText[contains(@text,'search_src_text')]")
//	private WebElement navigatetoSearch;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'National Water and Sewerage Company')]")
	private WebElement navigatetoNWSC;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@text,'Enter bill account number')]")
	private WebElement navigatetoEnterAccount;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'NEXT')]")
	private WebElement navigatetoNEXT;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@text,'Enter amount')]")
	private WebElement navigatetoEnterAmount;
	
	@FindBy(xpath = "//android.widget.Switch[contains(@text,'‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‎‎‏‏‏‎‏‎‏‏‏‎‎‏‎‏‏‎‏‎‏‏‎‏‎‏‏‎‎‏‎‏‏‎‎‏‏‎‎‎‏‏‎‎‎‎‏‏‏‎‏‎‎‎‎‎‏‎‎‏‎OFF‎‏‎‎‏‎')]")
	private WebElement navigatetoSwitchButton;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.LinearLayout[4]/android.widget.Button[2]")
	private WebElement navigatetoSendMoney;
	
	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.TextView")
	private WebElement navigatetoOK;

	//Pay Goods  and services
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Pay Goods & Services')]")
	private WebElement navigatetoPayGoods;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Enter Till Number')]")
	private WebElement navigatetoEnterTillNumber;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'till_number_edit')]")
	private WebElement navigatetoEnterTill;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'amount_edit')]")
	private WebElement navigatetoEnterAmountinTill;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'till_assitant')]")
	private WebElement navigatetoEnteradditional;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.Button[2]")
	private WebElement navigatetoBuyGoods;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.CheckBox")
	private WebElement navigatetoCheckbox;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.Button")
	private WebElement navigatetoOK1;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'fm_leftMenu')]")
	private WebElement navigatetoSideMenu;
	
	@FindBy(xpath = "//android.widget.RelativeLayout[contains(@resource-id,'log_out_row')]")
	private WebElement navigatetoLogout;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'next_button')]")
	private WebElement navigatetoNB;
	

	public void EazyPay(String account1, String amount1, String num1, String num2)
	{
		try
		{
			navigatetoLoginbutton.click();
			
			navigatetoPIN1.click();
			navigatetoPIN2.click();
			navigatetoPIN3.click();
			navigatetoPIN4.click();
			
//			try
//			{
//				navigatetoSkip.click();
//				navigatetoClose.click();
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
			navigatetoAddButton.click();
			
			navigatetoEazyPay.click();
			
			navigatetoPayBill.click();
			
			//Search function
//			driver.findElement(By.xpath("//android.widget.EditText[contains(@resource-id,'search_src_text')]")).sendKeys("C.P.C. staff welfare");
//			driver.pressKey(AndroidKeyCode.KEYCODE_PAGE_DOWN);
//    		driver.pressKey(AndroidKeyCode.KEYCODE_PAGE_DOWN);
//    		driver.pressKey(AndroidKeyCode.ENTER);
//    		driver.pressKey(AndroidKeyCode.ENTER);
//			Actions act = new Actions(driver);
//			WebElement el = driver
//					.findElement(By.xpath("//android.widget.EditText[contains(@resource-id,'search_src_text')]"));
//			el.sendKeys("C.P.C. staff welfare");
//			act.moveToElement(el).perform();
//			List el1=driver.findElements(By.xpath("//android.widget.LinearLayout[contains(@text,'C.P.C. staff welfare')]"));
//			for(int i=0;i<el1.size();i++)
//				System.out.println(el1.get(2));
//			Thread.sleep(2000);
//			
			navigatetoNWSC.click();
			
			navigatetoEnterAccount.sendKeys(account1);
			navigatetoNEXT.click();
			
			navigatetoEnterAmount.sendKeys(amount1);
			navigatetoNEXT.click();
			
			navigatetoSwitchButton.click();
			navigatetoSendMoney.click();
			
			navigatetoOK.click();
			
			//Pay Goods and Services
			navigatetoAddButton.click();
			
			navigatetoEazyPay.click();
			
			navigatetoPayGoods.click();
			
			navigatetoEnterTillNumber.click();
			
			navigatetoEnterTill.sendKeys(num1);
			navigatetoEnterAmountinTill.sendKeys(num2);
	
			navigatetoNEXT.click();
			driver.navigate().back();
			
			//Additional options
			navigatetoNB.click();
			
			navigatetoSwitchButton.click();
			navigatetoBuyGoods.click();
			
			navigatetoCheckbox.click();
			navigatetoOK1.click();
			
			navigatetoSideMenu.click();
			navigatetoLogout.click();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		}
}
