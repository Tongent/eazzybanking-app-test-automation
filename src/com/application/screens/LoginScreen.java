package com.application.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class LoginScreen {

	AndroidDriver<?> driver;
	
	public LoginScreen(AndroidDriver<?> driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'login')]")
	private WebElement navigatetoLoginbutton;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'1')]")
	private WebElement navigatetoPIN1;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'2')]")
	private WebElement navigatetoPIN2;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'3')]")
	private WebElement navigatetoPIN3;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'4')]")
	private WebElement navigatetoPIN4;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btnSkip')]")
	private WebElement navigatetoSkip;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'close_button')]")
	private WebElement navigatetoClose;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'fm_leftMenu')]")
	private WebElement navigatetoSideMenu;
	
	@FindBy(xpath = "//android.widget.LinearLayout[contains(@resource-id,'account_navigation_item')]")
	private WebElement navigatetoAccountSummary;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'LOAN ACCOUNTS')]")
	private WebElement navigatetoLoans;
	
	//Favourites
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Favourites')]")
	private WebElement navigatetoFavourites;
	
	//Remove
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Remove Favourite')]")
	private WebElement navigatetoRemove;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'60238972352345')]")
	private WebElement navigateto60238972352345;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'REMOVE')]")
	private WebElement navigatetoREMOVE;
	
	//ADD
	@FindBy(xpath = "//android.widget.ImageButton[contains(@resource-id,'fab_expand_menu_button')]")
	private WebElement navigatetoAdd;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Add Favourite')]")
	private WebElement navigatetoAddFavourite;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Equity Bank Account')]")
	private WebElement navigatetoEquityBank;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'Allow')]")
	private WebElement navigatetoAllow;
	
	@FindBy(xpath = "//android.widget.ImageButton[contains(@resource-id,'floating_dial_button')]")
	private WebElement navigatetoNumberpad;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'contact_number')]")
	private WebElement navigatetophone;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'NEXT')]")
	private WebElement navigatetoNext;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'ADD')]")
	private WebElement navigatetoADD;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'OK')]")
	private WebElement navigatetoOK;
	
	@FindBy(xpath = "//android.widget.RelativeLayout[contains(@resource-id,'log_out_row')]")
	private WebElement navigatetoLogout;
	
	public void login(String number)
	{
		try
		{
			navigatetoLoginbutton.click();
			
			navigatetoPIN1.click();
			navigatetoPIN2.click();
			navigatetoPIN3.click();
			navigatetoPIN4.click();
			
			navigatetoSideMenu.click();
			
			navigatetoAccountSummary.click();
			navigatetoLoans.click();
			
			Thread.sleep(3000);
			navigatetoSideMenu.click();
			navigatetoFavourites.click();
			
			//Remove favourites
			navigatetoAdd.click();
			Thread.sleep(1000);
			navigatetoRemove.click();
			
			navigateto60238972352345.click();
			
			navigatetoREMOVE.click();
			navigatetoOK.click();
			
			//Add favourites
			navigatetoAdd.click();
			Thread.sleep(1000);
			navigatetoAddFavourite.click();
			
			navigatetoEquityBank.click();
			navigatetoAllow.click();
			navigatetoNumberpad.click();
			
			navigatetophone.sendKeys(number);
			navigatetoNext.click();
			
			navigatetoADD.click();
			
			navigatetoOK.click();
			
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			navigatetoSideMenu.click();
			navigatetoLogout.click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
