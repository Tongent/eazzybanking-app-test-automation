package com.application.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class Buy_AirtimeScreens {

AndroidDriver<?> driver;
	
	public Buy_AirtimeScreens(AndroidDriver<?> driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Buy Airtime/Bundle')]")
	private WebElement navigatetoBuyAirTime;
	
	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")
	private WebElement navigatetoBuyAirTimeOption;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'SAFARICOM')]")
	private WebElement navigatetoSafaricom;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'Allow')]")
	private WebElement navigatetoAllow;
	
	@FindBy(xpath = "//android.widget.ImageButton[contains(@resource-id,'floating_dial_button')]")
	private WebElement navigatetoSendToDialPad;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'contact_number')]")
	private WebElement navigatetoContact;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'next_button')]")
	private WebElement navigatetoSendToNext;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'amount_edit')]")
	private WebElement navigatetoSendToAmount;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN1;

	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN2;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN3;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN4;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'0170100059651')]")
	private WebElement navigateto0170100059651;
	
	@FindBy(xpath = "//android.widget.Switch[contains(@text,'‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‎‎‏‏‏‎‏‎‏‏‏‎‎‏‎‏‏‎‏‎‏‏‎‏‎‏‏‎‎‏‎‏‏‎‎‏‏‎‎‎‏‏‎‎‎‎‏‏‏‎‏‎‎‎‎‎‏‎‎‏‎OFF‎‏‎‎‏‎')]")
	private WebElement navigatetoSwitch;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.Button[2]")
	private WebElement navigatetoBuyAirtime;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'OK')]")
	private WebElement navigatetoOK;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'fm_leftMenu')]")
	private WebElement navigatetoSideMenu;
	
	@FindBy(xpath = "//android.widget.RelativeLayout[contains(@resource-id,'log_out_row')]")
	private WebElement navigatetoLogout;
	
	public void Buy_Airtime(String contact, String amount) throws InterruptedException
	{
		navigatetoBuyAirTime.click();
		navigatetoBuyAirTimeOption.click();
		navigatetoSafaricom.click();
		//navigatetoAllow.click();
		
		navigatetoSendToDialPad.click();
		
		navigatetoContact.sendKeys(contact);
		navigatetoSendToNext.click();
		
		navigatetoSendToAmount.sendKeys(amount);
		navigatetoSendToNext.click();
		
		navigatetoPIN1.click();
		navigatetoPIN2.click();
		navigatetoPIN3.click();
		navigatetoPIN4.click();
		
		navigateto0170100059651.click();
		navigatetoSwitch.click();
		navigatetoBuyAirtime.click();
		navigatetoOK.click();
		
//		navigatetoSideMenu.click();
//		navigatetoLogout.click();
		
	}
}
