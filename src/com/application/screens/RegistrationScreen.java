package com.application.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import io.appium.java_client.android.AndroidDriver;

public class RegistrationScreen {
	
	AndroidDriver<?> driver;
	
	public RegistrationScreen(AndroidDriver<?> driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'grid_image_2')]")
	private WebElement navigatetoimage2;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btnNext')]")
	private WebElement navigatetoGetStarted;
	
	@FindBy(xpath = "//android.widget.FrameLayout[contains(@resource-id,'mbanking_customer')]")
	private WebElement navigatetoEQUITEL_EAZZY247;

	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'acc_first_name')]")
	private WebElement navigatetoFirstName;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'acc_last_name')]")
	private WebElement navigatetoLastName;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'identification_number')]")
	private WebElement navigatetoDocNumber;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btn_next')]")
	private WebElement navigatetoNEXT;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'select_country')]")
	private WebElement navigatetoSelectCountry;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@resource-id,'country_name')]")
	private WebElement navigatetoCountryDropdown;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'mobile_number')]")
	private WebElement navigatetoMobileNumber;
	
	//Wrong input field
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'mobile_number')]")
	private WebElement navigatetoIMobileNumber;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'account_number')]")
	private WebElement navigatetoAccountNumber;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'mbanking_pin')]")
	private WebElement navigatetoMPIN;
	
	@FindBy(xpath = "//android.widget.CheckBox[contains(@resource-id,'accept_terms')]")
	private WebElement navigatetoAgreeChekbox;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'next_button')]")
	private WebElement navigatetoNEXTBUTTON;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'button1')]")
	private WebElement navigatetoConfirmDetails;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'Allow')]")
	private WebElement navigatetoAllow;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'1')]")
	private WebElement navigatetoPIN1;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'2')]")
	private WebElement navigatetoPIN2;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'3')]")
	private WebElement navigatetoPIN3;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'4')]")
	private WebElement navigatetoPIN4;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'NEXT')]")
	private WebElement navigatetoNEXTPIN;
	
	//Negative scenario elements
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'5')]")
	private WebElement navigatetoNCPIN1;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'5')]")
	private WebElement navigatetoNCPIN5;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'6')]")
	private WebElement navigatetoNCPIN6;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'6')]")
	private WebElement navigatetoNCPIN7;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Ok')]")
	private WebElement navigatetoNOK;
	
	//Confirm pin
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'1')]")
	private WebElement navigatetoCPIN1;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'2')]")
	private WebElement navigatetoCPIN2;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'3')]")
	private WebElement navigatetoCPIN3;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'4')]")
	private WebElement navigatetoCPIN4;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btnOk')]")
	private WebElement navigatetoOK;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btnSkip')]")
	private WebElement navigatetoSkip;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'close_button')]")
	private WebElement navigatetoClose;
	
	//Register with EquityBank ATM CARD
	@FindBy(xpath="//android.widget.TextView[contains(@resource-id,'dc_lbl')]")
	private WebElement navigatetoEquityBankAtmCard;
	
	@FindBy(xpath="//android.widget.EditText[contains(@text,'Enter debit card number')]")
	private WebElement navigatetoDebitCard;
	
	@FindBy(xpath="//android.widget.Button[contains(@resource-id,'next_button')]")
	private WebElement navigatetoNext;
	
	@FindBy(xpath="//android.widget.EditText[contains(@text,'Enter account number for card')]")
	private WebElement navigatetoAccountNum;
	
	@FindBy(xpath="//android.widget.EditText[contains(@resource-id,'id_doc_number')]")
	private WebElement navigatetoIDDOC;
	
	@FindBy(xpath="//android.widget.EditText[contains(@resource-id,'phone_num')]")
	private WebElement navigatetoMobile;
	
	@FindBy(xpath="//android.widget.Button[contains(@resource-id,'contact_infor_next')]")
	private WebElement navigatetoN;
	
	@FindBy(xpath="//android.widget.EditText[contains(@text,'Enter card PIN')]")
	private WebElement navigatetoCardPin;
	
	//Register with I NEED HELP
	@FindBy(xpath="//android.widget.TextView[contains(@resource-id,'branch_onb_lbl')]")
	private WebElement navigatetoI_NEED_HELP;
	
	@FindBy(xpath="//android.widget.EditText[contains(@resource-id,'acc_account_number')]")
	private WebElement navigatetoBankAccount;
	
	@FindBy(xpath="//android.widget.Button[contains(@text,'NEXT')]")
	private WebElement navigatetoNEXT_H;
	
	@FindBy(xpath="//android.widget.TextView[contains(@resource-id,'select_id_doc')]")
	private WebElement navigatetoIDType;
	
	@FindBy(xpath="//android.widget.TextView[contains(@resource-id,'passport_number')]")
	private WebElement navigatetoPassport;
	
	@FindBy(xpath="//android.widget.EditText[contains(@resource-id,'id_num')]")
	private WebElement navigatetoIDNum;
	
	@FindBy(xpath="//android.widget.TextView[contains(@resource-id,'select_dob')]")
	private WebElement navigatetoDOB;
	
	@FindBy(xpath="//android.view.View[contains(@text,'21')]")
	private WebElement navigateto21;

	@FindBy(xpath="//android.widget.Button[contains(@text,'OK')]")
	private WebElement navigatetoOK1;
	
	@FindBy(xpath="//android.widget.EditText[contains(@resource-id,'email_address')]")
	private WebElement navigatetoEmail;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'fm_leftMenu')]")
	private WebElement navigatetoSideMenu;
	
	@FindBy(xpath = "//android.widget.RelativeLayout[contains(@resource-id,'log_out_row')]")
	private WebElement navigatetoLogout;
	
	
	public void RegistrationFlag(String fname, String lname, String DN, String M, String AN, String P, String IMN) throws Exception 
	{
		try
		{
			try
			{
			
			navigatetoimage2.click();
			navigatetoGetStarted.click();
			
			navigatetoEQUITEL_EAZZY247.click();		
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			navigatetoFirstName.sendKeys(fname);
			navigatetoLastName.sendKeys(lname);
			
			navigatetoDocNumber.sendKeys(DN);
			
			navigatetoNEXT.click();
			
			navigatetoSelectCountry.click();
			navigatetoCountryDropdown.click();
			
			navigatetoMobileNumber.sendKeys(M);
			
			navigatetoAccountNumber.sendKeys(AN);
			navigatetoMPIN.sendKeys(P);
			
			navigatetoNEXT.click();
			
			//Wrong input number
			navigatetoIMobileNumber.clear();
			navigatetoIMobileNumber.sendKeys(IMN);
			navigatetoNEXT.click();
	
			navigatetoAgreeChekbox.click();
			navigatetoNEXTBUTTON.click();
			navigatetoConfirmDetails.click();
			
			navigatetoAllow.click();
						
			Thread.sleep(5000);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			
			try
			{
				navigatetoPIN1.click();
				navigatetoPIN2.click();
				navigatetoPIN3.click();
				navigatetoPIN4.click();
				
				navigatetoNEXTPIN.click();
				
				//Negative
				navigatetoNCPIN1.click();
				navigatetoNCPIN5.click();
				navigatetoNCPIN6.click();
				navigatetoNCPIN7.click();
				navigatetoNEXTPIN.click();
				navigatetoNOK.click();
				
				//Re-entering 
				navigatetoPIN1.click();
				navigatetoPIN2.click();
				navigatetoPIN3.click();
				navigatetoPIN4.click();
				
				navigatetoNEXTPIN.click();
				
				navigatetoCPIN1.click();
				navigatetoCPIN2.click();
				navigatetoCPIN3.click();
				navigatetoCPIN4.click();

				navigatetoNEXTPIN.click();
				navigatetoOK.click();
			}
				catch(Exception e)
			{
					e.printStackTrace();
			}
			finally
			{
			navigatetoSkip.click();
			navigatetoClose.click();
			
			navigatetoSideMenu.click();
			
			navigatetoLogout.click();
			}
		}
	
		
		public void RegisterWithAtmCard(String card1, String account2, String Docnumber, String num, String cardPIN)
		{
			try
			{
				navigatetoimage2.click();
				navigatetoGetStarted.click();
				
				navigatetoEquityBankAtmCard.click();
				
				navigatetoDebitCard.sendKeys(card1);;
				navigatetoNext.click();
				navigatetoAccountNum.sendKeys(account2);
				navigatetoNext.click();
				
				navigatetoIDDOC.sendKeys(Docnumber);
				navigatetoSelectCountry.click();
				navigatetoCountryDropdown.click();
				navigatetoMobile.sendKeys(num);
				navigatetoN.click();
				
				navigatetoCardPin.sendKeys(cardPIN);
				navigatetoNext.click();
				
				navigatetoAgreeChekbox.click();
				navigatetoNext.click();
				
				navigatetoNext.click();
				Thread.sleep(3000);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		public void Registration_with_I_NEED_HELP(String name1, String name2, String name3, String name4, String name5)
		{
			try
			{
				navigatetoimage2.click();
				navigatetoGetStarted.click();
				
				navigatetoI_NEED_HELP.click();
				
				navigatetoFirstName.sendKeys(name1);
				navigatetoLastName.sendKeys(name2);
				navigatetoBankAccount.sendKeys(name3);
				
				navigatetoNEXT_H.click();
				navigatetoIDType.click();
				navigatetoPassport.click();
				navigatetoIDNum.sendKeys(name4);
				navigatetoDOB.click();
				navigateto21.click();
				navigatetoOK1.click();
				navigatetoNEXT_H.click();
				
				navigatetoSelectCountry.click();
				navigatetoCountryDropdown.click();
				navigatetoMobile.sendKeys(name5);
				
				//navigatetoEmail.sendKeys(name6);
				navigatetoNEXT_H.click();
				navigatetoAgreeChekbox.click();
				navigatetoNEXT_H.click();
				navigatetoNEXT_H.click();
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
	
	
	

}
