package com.application.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class LoansScreen {

AndroidDriver<?> driver;
	
	public LoansScreen(AndroidDriver<?> driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'login')]")
	private WebElement navigatetoLoginbutton;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'1')]")
	private WebElement navigatetoPIN1;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'2')]")
	private WebElement navigatetoPIN2;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'3')]")
	private WebElement navigatetoPIN3;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'4')]")
	private WebElement navigatetoPIN4;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'btnSkip')]")
	private WebElement navigatetoSkip;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'close_button')]")
	private WebElement navigatetoClose;
	
	@FindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'fm_leftMenu')]")
	private WebElement navigatetoSideMenu;
	
	@FindBy(xpath = "//android.widget.LinearLayout[contains(@resource-id,'loan_navigation_item')]")
	private WebElement navigatetoLoans;
	
	@FindBy(xpath="//android.widget.ImageButton[contains(@resource-id,'fab_expand_menu_button')]")
	private WebElement navigatetoAddButton;
	
	@FindBy(xpath="//android.widget.TextView[contains(@text,'Request Loan')]")
	private WebElement navigatetoRequestLoan;
	
	@FindBy(xpath="//android.widget.TextView[contains(@text,'0180190151342')]")
	private WebElement navigateto018;
	
	@FindBy(xpath="//android.widget.EditText[contains(@resource-id,'amount_edit')]")
	private WebElement navigatetoLoanAmount;	
	
	@FindBy(xpath="//android.widget.Button[contains(@resource-id,'next_button')]")
	private WebElement navigatetoNext;
	
	@FindBy(xpath="//android.widget.CheckBox[contains(@resource-id,'accept_terms')]")
	private WebElement navigatetoCheckbox;
	
	@FindBy(xpath="//android.widget.Button[contains(@text,'REQUEST LOAN')]")
	private WebElement navigatetoRequestLoanButton;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'OK')]")
	private WebElement navigatetoOK;
	
	//Check loan limit
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Check Loan Limit')]")
	private WebElement navigatetoCheckLoanLimit;

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Request Loan')]")
	private WebElement navigatetoRequestLoantab;
	
	//Make Payment
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Make Payment')]")
	private WebElement navigatetoMakePayment;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Equiloan')]")
	private WebElement navigatetoEquiLoan;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'MAKE PAYMENT')]")
	private WebElement navigatetoMakePayment1;
	
	//Send Money
	@FindBy(xpath = "//android.widget.Button[contains(@text,'Send Money')]")
	private WebElement navigatetoMakeSendMoney;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Transfer to Own Account')]")
	private WebElement navigatetoTransfer;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'0170190151342')]")
	private WebElement navigateto0170190151342;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'SEND MONEY')]")
	private WebElement navigatetoSendMoney;
	
	//Clear loan
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Clear Loan')]")
	private WebElement navigatetoClearLoan;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.Button[2]")
	private WebElement navigatetoClearLoanButton;
	
	//Total Loans
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Car Loan')]")
	private WebElement navigatetoTotalLoans_Car;
	
	@FindBy(xpath= "//android.widget.ImageView[contains(@resource-id,'fm_leftMenu')]")
	private WebElement menu;
	
	@FindBy(xpath = "//android.widget.RelativeLayout[contains(@resource-id,'log_out_row')]")
	private WebElement navigatetoLogout;
	
	
	public void loans(String amount1, String amount2, String amount3)
	{
		try
		{
			navigatetoLoginbutton.click();
			
			navigatetoPIN1.click();
			navigatetoPIN2.click();
			navigatetoPIN3.click();
			navigatetoPIN4.click();
			
//			navigatetoSkip.click();
//			navigatetoClose.click();
			
			navigatetoSideMenu.click();
			navigatetoLoans.click();
			
			navigatetoAddButton.click();
			navigatetoRequestLoan.click();
			
			navigateto018.click();
			navigatetoLoanAmount.sendKeys(amount1);
			navigatetoNext.click();
			
			navigatetoCheckbox.click();
			navigatetoRequestLoanButton.click();
			navigatetoOK.click();
			
			//Check loan limit and request the loan
			navigatetoAddButton.click();
			navigatetoCheckLoanLimit.click();
			navigatetoRequestLoantab.click();
			
			navigateto018.click();  
			navigatetoLoanAmount.sendKeys(amount1);
			navigatetoNext.click();
			
			navigatetoCheckbox.click();
			navigatetoRequestLoanButton.click();
			navigatetoOK.click();
			
			//Make Payment
			navigatetoAddButton.click();
			navigatetoMakePayment.click();
			Thread.sleep(2000);
			navigatetoEquiLoan.click();
			Thread.sleep(2000);
			navigateto018.click();
			navigatetoLoanAmount.sendKeys(amount2);
			navigatetoNext.click();
			
			navigatetoMakePayment1.click();
			navigatetoOK.click();
			
			//Clear Loan
			navigatetoAddButton.click();
			navigatetoClearLoan.click();
			Thread.sleep(2000);
			navigatetoEquiLoan.click();
			Thread.sleep(2000);
			navigateto018.click();
			
			navigatetoClearLoanButton.click();
			navigatetoOK.click();
			
			//Total Loans
			navigatetoTotalLoans_Car.click();
			navigatetoMakePayment.click();
			
			navigateto0170190151342.click();
			navigatetoLoanAmount.sendKeys(amount3);
			
			navigatetoNext.click();
			navigatetoMakePayment1.click();
			navigatetoOK.click();
			
			menu.click();
			navigatetoLogout.click();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			}
		}
}
