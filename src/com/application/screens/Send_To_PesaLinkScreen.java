	package com.application.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class Send_To_PesaLinkScreen {

AndroidDriver<?> driver;
	
	public Send_To_PesaLinkScreen(AndroidDriver<?> driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Send to PesaLink')]")
	private WebElement navigatetoPesaLink;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Send to phone')]")
	private WebElement navigatetoSendToPhone;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'Allow')]")
	private WebElement navigatetoSendToAllow;
	
	@FindBy(xpath = "//android.widget.ImageButton[contains(@resource-id,'floating_dial_button')]")
	private WebElement navigatetoSendToDialPad;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'contact_number')]")
	private WebElement navigatetoSendTonumbertextfield;
	
	@FindBy(xpath = "//android.widget.Button[contains(@resource-id,'next_button')]")
	private WebElement navigatetoSendToNext;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'amount_edit')]")
	private WebElement navigatetoSendToAmount;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'reason_edit')]")
	private WebElement navigatetoReason;
	
	//Send Money via kenya Commercial Bank
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Kenya Commercial BanK')]")
	private WebElement navigatetoKenyaCommercialBank;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN1;

	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN2;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN3;
	
	@FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[1]")
	private WebElement navigatetoPIN4;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'0170100059651')]")
	private WebElement navigateto0170100059651;
	
	@FindBy(xpath = "//android.widget.Switch[contains(@text,'‎‏‎‎‎‎‎‏‎‏‏‏‎‎‎‎‎‏‎‎‏‎‎‎‎‏‏‏‏‎‎‏‏‏‎‏‎‏‏‏‎‎‏‎‏‏‎‏‎‏‏‎‏‎‏‏‎‎‏‎‏‏‎‎‏‏‎‎‎‏‏‎‎‎‎‏‏‏‎‏‎‎‎‎‎‏‎‎‏‎OFF‎‏‎‎‏‎')]")
	private WebElement navigatetoSwitch;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'SEND MONEY')]")
	private WebElement navigatetoSendMoney;
	
	@FindBy(xpath = "//android.widget.Button[contains(@text,'OK')]")
	private WebElement navigatetoOK;
	
	//Equity bank
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Equity BanK')]")
	private WebElement navigatetoEquity_BanK;
	
	//Send to Account 
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Send to account')]")
	private WebElement navigatetoSendToAccount;
	
	@FindBy(xpath = "//android.widget.TextView[contains(@text,'Umeme Yaka')]")
	private WebElement navigatetoUmeme;
	
	@FindBy(xpath = "//android.widget.EditText[contains(@resource-id,'details_edit')]")
	private WebElement navigatetoAccount;
	
	

	public void PesaLink(String number, String amount, String reason, String account, String amount2) throws Exception 
	{
		try
		{
			navigatetoPesaLink.click();
			navigatetoSendToPhone.click();
			//navigatetoSendToAllow.click();
			navigatetoSendToDialPad.click();
			navigatetoSendTonumbertextfield.sendKeys(number);
			navigatetoSendToNext.click();
			
			navigatetoSendToAmount.sendKeys(amount);
			navigatetoSendToNext.click();
			
			navigatetoReason.sendKeys(reason);
			navigatetoSendToNext.click();
			
			//Send Money via kenya Commercial Bank
			navigatetoKenyaCommercialBank.click();
			
			navigatetoPIN1.click();
			navigatetoPIN2.click();
			navigatetoPIN3.click();
			Thread.sleep(1000);
			navigatetoPIN4.click();
			
			navigateto0170100059651.click();
			
			navigatetoSwitch.click();
			navigatetoSendMoney.click();
			
			navigatetoOK.click();
			
			
			//Same steps for Equity Bank
			navigatetoPesaLink.click();
			navigatetoSendToPhone.click();
			navigatetoSendToDialPad.click();
			navigatetoSendTonumbertextfield.sendKeys(number);
			navigatetoSendToNext.click();
			
			navigatetoSendToAmount.sendKeys(amount);
			navigatetoSendToNext.click();
			
			navigatetoReason.sendKeys(reason);
			navigatetoSendToNext.click();
			
			//Send Money via Equity Bank
			navigatetoEquity_BanK.click();
		
			navigatetoPIN1.click();
			navigatetoPIN2.click();
			navigatetoPIN3.click();
			Thread.sleep(1000);
			navigatetoPIN4.click();
			
			navigateto0170100059651.click();
			
			navigatetoSwitch.click();
			navigatetoSendMoney.click();
			
			navigatetoOK.click();
			
			
			//Send to Account - Scenario
			navigatetoPesaLink.click();
			
			navigatetoSendToAccount.click();
			
			navigatetoUmeme.click();
			
			navigatetoAccount.sendKeys(account);
			navigatetoSendToNext.click();
			
			navigatetoSendToAmount.sendKeys(amount2);
			navigatetoSendToNext.click();
			
			navigatetoReason.sendKeys(reason);
			navigatetoSendToNext.click();
			
			navigatetoPIN1.click();
			navigatetoPIN2.click();
			navigatetoPIN3.click();
			Thread.sleep(1000);
			navigatetoPIN4.click();
			
			navigateto0170100059651.click();
			
			navigatetoSwitch.click();
			navigatetoSendMoney.click();
			
			navigatetoOK.click();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		}
		
}
