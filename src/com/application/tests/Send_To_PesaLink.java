package com.application.tests;

import org.testng.annotations.Test;

import com.application.screens.Send_To_PesaLinkScreen;

public class Send_To_PesaLink extends BaseTest {

	// TestNG parameters as description, enable and Priority
	@Test(priority = 16, enabled = true, description = "launching the EquityBanking with PesaLink")
	public void testSend_To_Pesalink() throws Exception {

			test = extent.createTest("PesaLink");

			System.out.println("***********PesaLink***********");

			Send_To_PesaLinkScreen s = new Send_To_PesaLinkScreen(driver);
			
			  String PhoneNumber = excel.getData(4, 1, 0);
			  System.out.println("Phone number:" + PhoneNumber);
			  
			  String Amount = excel.getData(4, 1, 1);
			  System.out.println("Amount:" + Amount);
			  
			  String Text = excel.getData(4, 1, 2); 
			  System.out.println("Text:" + Text);
			  
			  String Account = excel.getData(4, 1, 3);
			  System.out.println("Account number:" + Account);
			  
			  String Amount1 = excel.getData(4, 1, 4);
			  System.out.println("Amount1:" + Amount1);
			 
			  s.PesaLink(PhoneNumber, Amount, Text, Account, Amount1);
			 
		}	
}
