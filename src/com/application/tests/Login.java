package com.application.tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.application.screens.LoginScreen;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class Login extends BaseTest {

	
	//TestNG parameters as description, enable and Priority
	@Test( priority=1, enabled= true, description= "launching the EquityBanking with Login")
	public void testLogin() throws Exception {
		test = extent.createTest("Login");
		System.out.println("***********Login***********");
		
		LoginScreen ls=new LoginScreen(driver);
		
		String Number=excel.getData(6, 1, 0);
		ls.login(Number);
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {

		if (result.getStatus() == ITestResult.FAILURE) {
			String temp = com.application.libraries.Utility.getScreenshot(driver, result.getName());

			test.fail("Something went wrong", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		}

		else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
		} else {
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
			test.skip(result.getThrowable());
		}
		extent.flush();
	}

	@AfterTest
	public void endReport() {
		driver.quit();
	}
		
}
