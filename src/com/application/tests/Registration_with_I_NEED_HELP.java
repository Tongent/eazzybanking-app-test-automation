package com.application.tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.application.screens.EazyPayScreen;
import com.application.screens.LoginScreen;
import com.application.screens.RegistrationScreen;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class Registration_with_I_NEED_HELP extends BaseTest{

	//TestNG parameters as description, enable and Priority
	@Test(priority=7, enabled= true, description= "launching the EquityBanking with registration via I NEED HELP")
	public void testRegistration_with_I_NEED_HELP() throws Exception {
		
		test = extent.createTest("Registration via I NEED HELP");
		System.out.println("***********Registration via I NEED HELP***********");
		
		//Calling SignupPage of com.application.tests.Registration package through import
		RegistrationScreen rs=new RegistrationScreen(driver);
		
		  String Firstname= excel.getData(2, 1, 0);
		  System.out.println("First_Name:" +Firstname);
		  
		  String Lastname= excel.getData(2, 1, 1);
		  System.out.println("Last_Name:" +Lastname);
		  
		  String BankAccount= excel.getData(2, 1, 2);
		  System.out.println("BankAccount:" +BankAccount);
		  
		  String IDNumber= excel.getData(2, 1, 3);
		  System.out.println("ID_Number:" +IDNumber);
		  
		  String Mobile= excel.getData(2, 1, 4);
		  System.out.println("Mobile_Number:" +Mobile);
		  
		  rs.Registration_with_I_NEED_HELP(Firstname, Lastname, BankAccount, IDNumber, Mobile);
		
		  String firstname= excel.getData(0, 1, 0);
		  System.out.println("First_Name:" +firstname);
		  
		  String lastname= excel.getData(0, 1, 1);
		  System.out.println("Last_Name:" +lastname);
		  
		  String Doc1= excel.getData(0, 1, 2);
		  System.out.println("IdentificationDocumentationNumber:" +Doc1);
		  
		  String Mobile1= excel.getData(0, 1, 3);
		  System.out.println("Wrong MobileNumber:" +Mobile1);
		  
		  String IMN=excel.getData(0, 2, 3);
		  System.out.println("MobileNumber:" +IMN);
		  
		  String Account1= excel.getData(0, 1, 4);
		  System.out.println("AccountNumber:" +Account1);
		  
		  String MPIN= excel.getData(0, 1, 5);
		  System.out.println("MPINNumber:" +MPIN);
		
		  rs.RegistrationFlag(firstname, lastname, Doc1, Mobile1, Account1, MPIN, IMN);
	}
	
	@Test( priority=8, enabled= true, description= "launching the EquityBanking with Login")
	public void testLogin() throws Exception {
		test = extent.createTest("Login");
		System.out.println("***********Login***********");
		
		LoginScreen ls=new LoginScreen(driver);
		
		String Number=excel.getData(6, 1, 0);
		ls.login(Number);
	}
	
	@Test(priority = 9, enabled = true, description = "Launching the EazzyPay")
	public void testRegistration() throws Exception {
		test = extent.createTest("Eazypay");
		System.out.println("***********Eazypay***********");

		EazyPayScreen ep = new EazyPayScreen(driver);

		
		String Account=excel.getData(3, 1, 0);
		  System.out.println("Account number:"+Account);
		  
		  String Amount=excel.getData(3, 1 , 1);
		  System.out.println("Amount :"+Amount);
		  
		  String EnterTill=excel.getData(3, 1, 2);
		  System.out.println("E number:"+EnterTill);
		  
		  String EnterAmount=excel.getData(3, 1, 3);
		  System.out.println("Enter Till Amount :"+EnterAmount);
		  
		  ep.EazyPay(Account, Amount, EnterTill, EnterAmount);
		 
	}
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {

		if (result.getStatus() == ITestResult.FAILURE) {
			String temp = com.application.libraries.Utility.getScreenshot(driver, result.getName());

			test.fail("Something went wrong", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		}

		else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
		} else {
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
			test.skip(result.getThrowable());
		}
		extent.flush();
	}

	@AfterTest
	public void endReport() {
		driver.quit();
	}
}
