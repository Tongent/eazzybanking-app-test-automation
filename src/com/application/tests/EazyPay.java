package com.application.tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.application.screens.EazyPayScreen;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class EazyPay extends BaseTest {

	// TestNG parameters as description, enable and Priority
	@Test(priority = 10, enabled = true, description = "Launching the EazzyPay")
	public void testRegistration() throws Exception {
		test = extent.createTest("Eazypay");
		System.out.println("***********EazzyPay***********");

		EazyPayScreen ep = new EazyPayScreen(driver);

		
		String Account=excel.getData(3, 1, 0);
		  System.out.println("Account number:"+Account);
		  
		  String Amount=excel.getData(3, 1, 2);
		  System.out.println("Amount :"+Amount);
		  
		  String EnterTill=excel.getData(3, 1, 3);
		  System.out.println("E number:"+EnterTill);
		  
		  String EnterAmount=excel.getData(3, 1, 4);
		  System.out.println("Enter Till Amount :"+EnterAmount);
		  
		  ep.EazyPay(Account, Amount, EnterTill, EnterAmount);
		 
	}
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {

		if (result.getStatus() == ITestResult.FAILURE) {
			String temp = com.application.libraries.Utility.getScreenshot(driver, result.getName());

			test.fail("Something went wrong", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		}

		else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
		} else {
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
			test.skip(result.getThrowable());
		}
		extent.flush();
	}

	@AfterTest
	public void endReport() {
		driver.quit();
	}

	
}
