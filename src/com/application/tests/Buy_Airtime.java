package com.application.tests;

import org.testng.annotations.Test;

import com.application.screens.Buy_AirtimeScreens;

public class Buy_Airtime extends BaseTest{
	
			//TestNG parameters as description, enable and Priority
			@Test(priority=14, enabled= true, description= "launching the EquityBanking with BuyAirtime")
			public void testSend_To_Pesalink() throws Exception {
				
				test = extent.createTest("Buy Airtime");
				System.out.println("***********Buy Airtime***********");
			
				Thread.sleep(3000);
				Buy_AirtimeScreens b=new Buy_AirtimeScreens(driver);
				
				  String Contant= excel.getData(5, 1, 0);
				  System.out.println("ContactNumber:" +Contant);
				  
				  String Amount= excel.getData(5, 1, 1);
				  System.out.println("AmountNumber:" +Amount);
				  
				  b.Buy_Airtime(Contant, Amount);
				 
			}
}
