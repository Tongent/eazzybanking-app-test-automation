package com.application.tests;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.application.libraries.ReadExcelConfig;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseTest {

	public static AndroidDriver<?> driver;
	
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	public static ExtentTest logger;
	public static ReadExcelConfig excel;
	
	@BeforeSuite
	public void launch() throws MalformedURLException, InterruptedException
	{
		File app=new File(".\\app\\app-mock-debug.apk");
		DesiredCapabilities cap=new DesiredCapabilities();
		
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "RR8M70FA86A");
		cap.setCapability(MobileCapabilityType.APP,app.getAbsolutePath());
		cap.setCapability("platformName", "Android");
		
		driver=new AndroidDriver<>(new URL("http://0.0.0.0:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		excel = new ReadExcelConfig("./Data/Input_Data.xlsx");
		System.out.println("Successfully read XL sheet");
		File testDirectory = new File(System.getProperty("user.dir")+"/reports/Automation.html");
		boolean result = false;
		if (!testDirectory.exists()) {
		System.out.println("creating directory: " + testDirectory.getName());

		try {
		testDirectory.createNewFile();
		result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result) {
		System.out.println("DIR created");
		}
		}
			
		
		htmlReporter = new ExtentHtmlReporter("./reports/Automation.html");
		
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		extent.setSystemInfo("OS", "Windows 10");
		extent.setSystemInfo("TesterName", "Pradeep");
		extent.setSystemInfo("Environment", "QA");

		htmlReporter.config().setChartVisibilityOnOpen(true);
		htmlReporter.config().setDocumentTitle("AutomationTesting.in Demo Report");
		htmlReporter.config().setReportName("My Own Report");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
	}
	
	
	  @AfterMethod 
	  public void getResult(ITestResult result) 
	  {
		  if(result.getStatus() == ITestResult.FAILURE) 
		  {  
			  test.log(Status.FAIL,
	  MarkupHelper.createLabel(result.getName() +
	  "Test case FAILED due to below issues:", ExtentColor.RED));
	  test.fail(result.getThrowable()); 		  } else if (result.getStatus() ==
	  ITestResult.SUCCESS) { test.log(Status.PASS,
	  MarkupHelper.createLabel(result.getName() + "Test Case PASSED",
	  ExtentColor.GREEN)); } else { test.log(Status.SKIP,
	  MarkupHelper.createLabel(result.getName() + "Test Case SKIPPED",
	  ExtentColor.ORANGE)); test.skip(result.getThrowable());} 
	  }
	 
	
	  @AfterSuite
		public void tearDown() {
			extent.flush();
		driver.close();
	}
}