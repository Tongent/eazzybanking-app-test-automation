package com.application.tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.application.screens.LoansScreen;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class Loans extends BaseTest{
	
	//TestNG parameters as description, enable and Priority
		@Test(priority=12, enabled= true, description= "launching the EquityBanking wih Loans")
		public void test() throws Exception {
			
			test = extent.createTest("Loans");
			System.out.println("***********Loans***********");
			
			LoansScreen ln=new LoansScreen(driver);
			
			  String Amount1=excel.getData(7, 1, 0);
			  System.out.println("Amount 1:" +Amount1);
			  
			  String Amount2= excel.getData(7, 1, 1);
			  System.out.println("Amount 2:" +Amount2);
			  
			  String Amount3= excel.getData(7, 1, 2);
			  System.out.println("Amount 3:" +Amount3);
			  
			  ln.loans(Amount1, Amount2, Amount3);
			
		}
		
		@AfterMethod
		public void tearDown(ITestResult result) throws IOException {

			if (result.getStatus() == ITestResult.FAILURE) {
				String temp = com.application.libraries.Utility.getScreenshot(driver, result.getName());

				test.fail("Something went wrong", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
			}

			else if (result.getStatus() == ITestResult.SUCCESS) {
				test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
			} else {
				test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
				test.skip(result.getThrowable());
			}
			extent.flush();
		}

		@AfterTest
		public void endReport() {
			driver.quit();
		}
}
